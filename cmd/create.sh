rename_function cmd_create orig_cmd_create
cmd_create() {
    chmod +x .
    mkdir -p postgresql config
    orig_cmd_create \
        --mount type=bind,src=$(pwd)/postgresql,dst=/var/lib/postgresql \
        --mount type=bind,src=$(pwd)/config,dst=/opt/liquid_feedback_frontend/config \
        "$@"
}

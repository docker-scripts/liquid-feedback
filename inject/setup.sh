#!/bin/bash -x

source /host/settings.sh

main() {
    setup_db
    setup_frontend
    setup_services
}

setup_db() {
    # check whether the db already exists
    if [[ -n $(ls /var/lib/postgresql) ]]; then
        chown postgres -R /var/lib/postgresql/
        return
    fi

    # make sure that the service is running
    cp -a /var/lib/postgresql.initial/* /var/lib/postgresql/
    chown postgres -R /var/lib/postgresql/
    systemctl start postgresql
    
    # ensure that the user account of your webserver has access to the database
    su - postgres -c 'createuser --no-superuser --createdb --no-createrole www-data'

    # create a database
    su www-data -s $SHELL <<EOF
createdb liquid_feedback
psql -v ON_ERROR_STOP=1 -f /opt/liquid_feedback_core/core.sql liquid_feedback

psql liquid_feedback <<EOF1
INSERT INTO system_setting (member_ttl) VALUES ('1 year');
INSERT INTO contingent (polling, time_frame, text_entry_limit, initiative_limit) VALUES (false, '1 hour', 20, 6);
INSERT INTO contingent (polling, time_frame, text_entry_limit, initiative_limit) VALUES (false, '1 day', 80, 12);
INSERT INTO contingent (polling, time_frame, text_entry_limit, initiative_limit) VALUES (true, '1 hour', 200, 60);
INSERT INTO contingent (polling, time_frame, text_entry_limit, initiative_limit) VALUES (true, '1 day', 800, 120);
INSERT INTO member (invite_code, admin) VALUES ('sesam', true);
EOF1

EOF
}

setup_frontend() {
    local frontend=/opt/liquid_feedback_frontend
    # make 'tmp/' dir writable for webserver
    chown www-data $frontend/tmp

    # configure the LiquidFeedback-Frontend
    if [[ -z $(ls $frontend/config/) ]]; then
        cp -a $frontend/config.initial/* $frontend/config/
        cp $frontend/config/{example.lua,myconfig.lua}
        sed -i $frontend/config/myconfig.lua \
            -e "/config\.absolute_base_url/c config.absolute_base_url = 'https://$DOMAIN/'" \
            -e "/config\.localhost/c config.localhost = false" \
            -e "/config\.public_access/c config.public_access = 'everything'" \
            -e "/config\.delegation_warning_time/c config.delegation_warning_time = '6 months'" \
            -e "/config\.check_delegations_interval_soft/c config.check_delegations_interval_soft = '3 months'" \
            -e "/config\.check_delegations_interval_hard/c config.check_delegations_interval_hard = '6 months'"
    fi
}

setup_services() {
    # Setup regular execution of 'lf_update' and related commands
    cat > /opt/liquid_feedback_core/lf_update.sh <<EOF
#!/bin/sh

while true; do
  nice /opt/liquid_feedback_core/lf_update dbname=liquid_feedback 2>&1 | logger -t "lf_core"
  nice /opt/liquid_feedback_core/lf_update_issue_order dbname=liquid_feedback 2>&1 | logger -t "lf_core"
  nice /opt/liquid_feedback_core/lf_update_suggestion_order dbname=liquid_feedback 2>&1 | logger -t "lf_core"
  sleep 5
done
EOF
    chmod +x /opt/liquid_feedback_core/lf_update.sh

    cat > /etc/systemd/system/liquid_feedback_core.service <<EOF
[Unit]
Description=LiquidFeedback Core update

[Service]
User=www-data
ExecStart=/opt/liquid_feedback_core/lf_update.sh

[Install]
WantedBy=multi-user.target
EOF
    systemctl daemon-reload
    systemctl enable liquid_feedback_core
    systemctl start liquid_feedback_core

    # Start the frontend
    cat > /opt/liquid_feedback_frontend/run.sh <<EOF
#!/bin/bash

/opt/moonbridge/moonbridge /opt/webmcp/bin/mcp.lua /opt/webmcp/ /opt/liquid_feedback_frontend/ main myconfig  2>&1 | logger -t "lf_frontend"
EOF
    chmod +x /opt/liquid_feedback_frontend/run.sh

    cat > /etc/systemd/system/liquid_feedback_frontend.service <<EOF
[Unit]
Description=LiquidFeedback Frontend

[Service]
User=www-data
ExecStart=/opt/liquid_feedback_frontend/run.sh

[Install]
WantedBy=multi-user.target
EOF
    systemctl daemon-reload
    systemctl enable liquid_feedback_frontend
    systemctl start liquid_feedback_frontend
}

# call main
main "$@"

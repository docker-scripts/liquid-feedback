# Liquid-Feedback in a container

https://liquidfeedback.com/en/open-source.html

## Installation

  - First install `ds` and `revproxy`:
      + https://gitlab.com/docker-scripts/ds#installation
      + https://gitlab.com/docker-scripts/revproxy#installation

  - Then get the scripts: `ds pull liquid-feedback`

  - Create a directory for the container: `ds init liquid-feedback @feedback.example.org`

  - Fix the settings: `cd /var/ds/feedback.example.org/ ; vim settings.sh`

  - Make the container: `ds make`

  - Edit the frontend configuration: `vim config/myconfig.lua`

  - Restart: `ds restart`
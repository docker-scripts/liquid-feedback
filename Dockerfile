include(bookworm)

RUN apt update && apt upgrade --yes
RUN DEBIAN_FRONTEND=noninteractive \
    apt install --yes \
        build-essential \
        postgresql \
        postgresql-server-dev-15 \
        libbsd-dev \
        lua5.3 \
        liblua5.3-dev \
        mercurial \
        bmake \
        lsb-release \
        imagemagick \
        sassc \
        wget
RUN cp -a /var/lib/postgresql /var/lib/postgresql.initial

ENV ARCHIVE_URL https://www.public-software-group.org/pub/projects

### Install LiquidFeedback-Core
RUN wget $ARCHIVE_URL/liquid_feedback/backend/v4.2.2/liquid_feedback_core-v4.2.2.tar.gz &&\
    tar xfz liquid_feedback_core-v4.2.2.tar.gz &&\
    cd liquid_feedback_core-v4.2.2/ &&\
    make &&\
    mkdir -p /opt/liquid_feedback_core &&\
    cp core.sql lf_update lf_update_issue_order lf_update_suggestion_order /opt/liquid_feedback_core &&\
    cd .. &&\
    rm -rf liquid_feedback_core-v4.2.2/ &&\
    rm -f liquid_feedback_core-v4.2.2.tar.gz

### Install Moonbridge
RUN wget $ARCHIVE_URL/moonbridge/v1.1.3/moonbridge-v1.1.3.tar.gz &&\
    tar xfz moonbridge-v1.1.3.tar.gz &&\
    cd moonbridge-v1.1.3/ &&\
    pmake MOONBR_LUA_PATH=/opt/moonbridge/?.lua &&\
    mkdir -p /opt/moonbridge &&\
    cp moonbridge /opt/moonbridge/ &&\
    cp moonbridge_http.lua /opt/moonbridge/ &&\
    cd .. &&\
    rm -rf moonbridge-v1.1.3/ &&\
    rm -f moonbridge-v1.1.3.tar.gz

### Install WebMCP
RUN wget $ARCHIVE_URL/webmcp/v2.2.1/webmcp-v2.2.1.tar.gz &&\
    tar xfz webmcp-v2.2.1.tar.gz &&\
    cd webmcp-v2.2.1/ &&\
    make &&\
    mkdir -p /opt/webmcp &&\
    cp -RL framework/* /opt/webmcp/ &&\
    cd .. &&\
    rm -rf webmcp-v2.2.1/ &&\
    rm -f webmcp-v2.2.1.tar.gz

### Install the LiquidFeedback-Frontend
RUN wget $ARCHIVE_URL/liquid_feedback/frontend/v4.0.0/liquid_feedback_frontend-v4.0.0.tar.gz &&\
    tar xfz liquid_feedback_frontend-v4.0.0.tar.gz &&\
    mv liquid_feedback_frontend-v4.0.0 /opt/liquid_feedback_frontend &&\
    cp -a /opt/liquid_feedback_frontend/config /opt/liquid_feedback_frontend/config.initial &&\
    chown www-data /opt/liquid_feedback_frontend/tmp &&\
    rm -f liquid_feedback_frontend-v4.0.0.tar.gz

RUN unset ARCHIVE_URL
